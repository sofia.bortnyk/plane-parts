(function($) {
	if($('.product__slider').length) {
		$('.product__slider').slick({
			prevArrow: $('.product__arrows:not(.related__arrows) .product__prev'),
			nextArrow: $('.product__arrows:not(.related__arrows) .product__next')
		});
	}

	$('.product__tab').on('click',function(){
		if(!$(this).hasClass('active')) {
			var index = $(this).index();
			$('.product__tab').removeClass('active');
			$(this).addClass('active');
			$('.product__tabs-content').removeClass('active');
			$('.product__tabs-content').eq(index).addClass('active');
		}
	});

	$('.cat__sidebar-name').on('click',function(){
		$(this).closest('.cat__sidebar-filter').toggleClass('opened');
	});

	$('.first-section').on('mousemove',function(e){
		var x = e.clientX;
		var y = e.clientY;
		var middle = $(window).width()/2;
		$('.first-section__bg').css('transform','translate('+x/50+'px,'+y/50+'px)');
	});

	$('.first-section').on('mousemove',function(e){
		var x = e.clientX;
		var y = e.clientY;
		var middle = $(window).width()/2;
		$('.first-section__plane').css('transform','translate(calc(-50% + '+x/25+'px),'+y/25+'px)');
	});

	$('.cart__minus').on('click',function(){
		var val = parseInt($(this).closest('.cart__quantity').find('input').val())-1;
		if(val >= 0) {
			$(this).closest('.cart__quantity').find('input').val(val);	
		}
	});

	$('.cart__plus').on('click',function(){
		var val = parseInt($(this).closest('.cart__quantity').find('input').val())+1;
		$(this).closest('.cart__quantity').find('input').val(val);
	});

	$('.popup__form-close').on('click',function(){
		$('.popup').fadeOut();
	});

	$('.ask-btn').on('click',function(e){
		e.preventDefault();
		$('.popup').fadeIn();
	});

	$('.header__mobile-btn').on('click',function(){
		$(this).toggleClass('active');
		$('.mobile-menu').slideToggle();
	});

	if(window.matchMedia("(max-width: 991px)").matches && $('.related__list').length) {
		$('.related__list').slick({
			slidesToShow: 3,
			prevArrow: $('.related__arrows .product__prev'),
			nextArrow: $('.related__arrows .product__next'),
			responsive: [
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						prevArrow: $('.related__arrows .product__prev'),
						nextArrow: $('.related__arrows .product__next'),
					}
				},
				{
					breakpoint: 459,
					settings: {
						slidesToShow: 1,
						prevArrow: $('.related__arrows .product__prev'),
						nextArrow: $('.related__arrows .product__next'),
					}
				}
			]
		});
	}

	if(window.matchMedia("(max-width: 1199px)").matches) {
		$('.cat__sidebar').on('click',function(e){
			if(e.target === this || $(e.target).hasClass('cat__sidebar-title')) {
				$(this).toggleClass('opened');
			}
		});
	}

	window.addEventListener("orientationchange", function() {
	  if(window.matchMedia("(max-width: 991px)").matches && $('.related__list').length) {
			$('.related__list').slick({
				slidesToShow: 3,
				prevArrow: $('.related__arrows .product__prev'),
				nextArrow: $('.related__arrows .product__next'),
				responsive: [
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 2,
							prevArrow: $('.related__arrows .product__prev'),
							nextArrow: $('.related__arrows .product__next'),
						}
					},
					{
						breakpoint: 459,
						settings: {
							slidesToShow: 1,
							prevArrow: $('.related__arrows .product__prev'),
							nextArrow: $('.related__arrows .product__next'),
						}
					}
				]
			});
		}

		if(window.matchMedia("(max-width: 1199px)").matches) {
			$('.cat__sidebar').on('click',function(e){
				if(e.target === this || $(e.target).hasClass('cat__sidebar-title')) {
					$(this).toggleClass('opened');
				}
			});
		}
	});

	$(window).on('resize',function(){
		if(window.matchMedia("(max-width: 991px)").matches && $('.related__list').length) {
			$('.related__list').slick({
				slidesToShow: 3,
				prevArrow: $('.related__arrows .product__prev'),
				nextArrow: $('.related__arrows .product__next'),
				responsive: [
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 2,
							prevArrow: $('.related__arrows .product__prev'),
							nextArrow: $('.related__arrows .product__next'),
						}
					},
					{
						breakpoint: 459,
						settings: {
							slidesToShow: 1,
							prevArrow: $('.related__arrows .product__prev'),
							nextArrow: $('.related__arrows .product__next'),
						}
					}
				]
			});
		}

		if(window.matchMedia("(max-width: 1199px)").matches) {
			$('.cat__sidebar').on('click',function(e){
				if(e.target === this || $(e.target).hasClass('cat__sidebar-title')) {
					$(this).toggleClass('opened');
				}
			});
		}
	});

})( jQuery );